from django.db import models
from django.contrib.auth import get_user_model

class workouts(models.Model):   
    image = models.URLField(max_length=200)
    instructions = models.TextField()
    local = models.BooleanField(default=False)


class user_session(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    last_activity = models.DateTimeField()
    mood = models.IntegerField()
    user_id = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

class user_weight(models.Model):
    user_id = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    user_weight = models.DecimalField(max_digits=5, decimal_places=2)

class user_workouts(models.Model):
    session_id = models.ForeignKey(user_session, on_delete=models.CASCADE)
    workout_id = models.ForeignKey(workouts, on_delete=models.CASCADE)
    order = models.IntegerField()
    
class user_set(models.Model):
    workout_id = models.ForeignKey(user_workouts, on_delete=models.CASCADE)
    rest_time = models.DurationField()
    #TODO Validation 
    weight = models.TextField()
    reps = models.TextField()