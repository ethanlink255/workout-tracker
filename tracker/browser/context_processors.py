from django.http import request

def add_variable_to_context(request):
    if request.user.is_authenticated:
        return {
             'user': "Hello {0}".format(request.user.username)
        }
    else:
        return {
            'user': "Login"
        }